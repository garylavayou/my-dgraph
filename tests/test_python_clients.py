import json
import unittest
from time import sleep

import pydgraph
from clients import python_client


class TestPythonClient(unittest.TestCase):

    def setUp(self) -> None:
        self.stub = pydgraph.DgraphClientStub('localhost:9080')
        self.client = pydgraph.DgraphClient(self.stub)
        self.node = {
            'x.name': 'Alice',
            'x.age': 30,
            'x.email': 'alice@dgraph.io'
        }
        self.query = '''
        {
            person(func: eq(x.name, "Alice"), first: 10){
                uid
                name: x.name
                age: x.age
                email: x.email
            }
        }
        '''
        self.update_query = '''
        {
            person(func: eq(x.email, "gary@dgraph.io"), first: 10){
                uid
                name: x.name
                email: x.email
            }
        }
        '''
        self.delay = 1  # should be long enough to avoid inconsistent state
        print('')

    def tearDown(self) -> None:
        self.stub.close()

    def test_alter_schema(self):
        schema = 'test.pydgraph.name: string @index(exact) .'
        python_client.alter_schema(self.client, schema)

    def do_test_query(self, q: str = None):
        if q is None:
            q = self.query
        sleep(self.delay)
        r = python_client.query(self.client, q)
        data = json.loads(r.json)['person']
        n = len(data)
        return n

    def do_clean_node(self):
        # remove exist node
        r = python_client.delete(
            self.client, 'x.name', 'Alice',
            fields=['x.name', 'x.age', 'x.email']
        )
        # re-check deletion status
        n = self.do_test_query()
        self.assertEqual(n, 0, f'{n} nodes found!')

    def do_insert_node(self):
        r = python_client.insert(self.client, 'x.name', 'Alice', obj=self.node)
        n = len(r.uids)
        self.assertEqual(n, 1, f'node insertion failed!')

    def test_query(self):
        self.do_clean_node()
        self.do_insert_node()
        sleep(self.delay)
        response = python_client.query(self.client, self.query)
        print(response)
        n = len(json.loads(response.json)['person'])
        self.assertEqual(n, 1, 'query failed.')

    def test_insert_none(self):
        self.do_clean_node()
        r = python_client.insert(self.client, 'x.name', 'Alice', obj=self.node)
        print(r)
        n = len(r.uids)
        self.assertEqual(n, 1, f'node insertion failed!')
        n = self.do_test_query()
        self.assertEqual(n, 1, f'node insertion failed!')

    def test_insert_exist(self):
        self.do_clean_node()
        self.do_insert_node()
        r = python_client.insert(self.client, 'x.name', 'Alice', obj=self.node)
        print(r)
        n = len(r.uids)
        self.assertEqual(n, 0, f'node insertion not detect duplicates!')
        n = self.do_test_query()
        self.assertEqual(n, 1, f'node insertion not detect duplicates!')

    def test_insert_nquads(self):
        nquads = '''
        _:alice <x.name> "Alice" .
        _:alice <x.email> "alice@dgraph.io" .
        _:alice <x.age> "32" .
        '''
        self.do_clean_node()
        r = python_client.insert(self.client, 'x.name', 'Alice', nquads=nquads)
        n = len(r.uids)
        self.assertEqual(n, 1, f'node insertion failed!')
        n = self.do_test_query()
        self.assertEqual(n, 1, f'node insertion failed!')

    def test_update_none(self):
        self.do_clean_node()
        r = python_client.update(self.client, 'x.name', 'Alice', obj=self.node)
        print(r)
        n = self.do_test_query()
        self.assertEqual(n, 0, f'update an non-exist node should failed!')

    def test_update_exist(self):
        self.do_clean_node()
        self.do_insert_node()
        self.node['x.email'] = 'gary@dgraph.io'
        r = python_client.update(self.client, 'x.name', 'Alice', obj=self.node)
        print(r)
        # check update results
        n = self.do_test_query(q=self.update_query)
        self.assertEqual(n, 1, f'node update failed!')

    def test_upsert_create(self):
        self.do_clean_node()
        r = python_client.upsert(self.client, 'x.name', 'Alice', obj=self.node)
        n = len(r.uids)
        self.assertEqual(n, 1, f'node insertion failed!')

        n = self.do_test_query()
        self.assertEqual(n, 1, f'node insertion failed!')

    def test_upsert_update(self):
        self.do_clean_node()
        self.do_insert_node()
        self.node['x.email'] = 'gary@dgraph.io'
        r = python_client.upsert(self.client, 'x.name', 'Alice', obj=self.node)
        print(r)
        n = self.do_test_query(q=self.update_query)
        self.assertEqual(n, 1, f'node update failed!')

    def test_delete(self):
        self.do_insert_node()
        r = python_client.delete(
            self.client, 'x.name', 'Alice',
            fields=['x.name', 'x.age', 'x.email']
        )
        print(r)
        n = self.do_test_query()
        self.assertEqual(n, 0, f'{n} nodes found!')

    def test_delete_json(self):
        self.do_insert_node()
        r = python_client.delete(
            self.client, 'x.name', 'Alice',
            fields=['x.name', 'x.age', 'x.email'],
            format='json'
        )
        print(r)
        n = self.do_test_query()
        self.assertEqual(n, 0, f'{n} nodes found!')


class DgraphQueryTest(unittest.TestCase):
    def test_create_argument(self):
        arg = python_client.Argument(
            name='var_name', type='string', default="test")
        print(arg)


if __name__ == '__main__':
    unittest.main()
