#!/bin/bash
# only for uploading small scale data
# use dgraph live to upload large scale dataset.
# ./bin/dgraph live --files examples/1million.rdf --new_uids --xidmap ./xids

function usage(){
    echo "usage: upload.sh --format {json|rdf} --file /path/to/file"
    exit 1
}

SHORT='f:F:'
LONGS='file:,format:'
! PARSED=$(getopt --options=$SHORT --longoptions=$LONGS --name "$0" -- "$@") #*
if [ ${PIPESTATUS[0]} -ne 0 ]; then  # [ $? -ne 0 ]
    exit 2
fi
eval set -- "$PARSED"  #**
while true; do
    case "$1" in
        -f|--file)
        	DATA_BINARY=$2
            shift
            ;;
        -F|--format)
        	DATA_FORMAT=$2
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            usage
            exit 1
            ;;
    esac
    shift
done
DATA_BINARY=${DATA_BINARY:-"$1"}
if [ -z "$DATA_BINARY" ]; then
    echo "error: data source not specified!"
    usage
fi
if [ -z "$DATA_FORMAT" ]; then
    DATA_FORMAT=$(sed -En 's/.*\.(.*?)$/\1/p' <<< $DATA_BINARY )
fi
if [[ ! $DATA_FORMAT =~ rdf|json ]]; then
    echo "error: unknown data format '$DATA_FORMAT'."
    usage
fi
# echo $DATA_BINARY $DATA_FORMAT

curl -XPOST "http://localhost:8080/mutate?commitNow=true" --silent \
     --header "Content-Type: application/$DATA_FORMAT" \
     --data-binary "@${DATA_BINARY}"