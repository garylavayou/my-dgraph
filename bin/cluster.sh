#!/bin/bash
set -e

COMMAND=${1:-'start'}
if [[ "$COMMAND" =~ start|stop ]]; then
    for node in zero alpha ratel; do
        systemctl $COMMAND dgraph-$node.service
    done
elif [[ "$COMMAND" == 'status' ]]; then
    systemctl status dgraph-*.service
elif [[ "$COMMAND" == 'ps' ]]; then
    ps -ef | grep /bin/dgraph | grep -v grep
fi