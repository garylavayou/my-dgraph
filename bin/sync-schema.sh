#!/bin/bash

COMMAND=${1:-'read'}
SCHEMA_FILE=${2:-'/opt/dgraph/examples/schema.graphql'}

DL_CLIENT="curl -XPOST --silent 'http://localhost:8080/admin' 
                --header 'Content-Type: application/graphql' 
                --data '{getGQLSchema{schema}}' |
                jq --raw-output '.data.getGQLSchema.schema'"
UL_CLIENT="curl -XPOST --silent http://localhost:8080/admin/schema 
                --header 'Content-Type: application/graphql'"
# set -x
case $COMMAND in
read)
    eval $DL_CLIENT 
    ;;
download)
    eval $DL_CLIENT > $SCHEMA_FILE
    ls -lht $SCHEMA_FILE
    ;;
upload)
    eval $UL_CLIENT --data-binary "@${SCHEMA_FILE}"
    ;;
alter)
    SCHEMA_FILE=${2:?'file not specified'}
    curl -XPOST "http://localhost:8080/alter" \
         --data-binary "@${SCHEMA_FILE}"
    ;;
*)
    echo "error: unkown command [${COMMAND}]."
    exit 1
    ;;
esac

echo ""