'''
conda create -n dgraph 'python=3.10' autopep8 rich
conda activate dgraph
pip install pydgraph 'protobuf==3.20'
'''
import contextlib
import re

from rich import print

import pydgraph
from pydgraph import DgraphClient, Txn

from .transaction import Transaction


def alter_schema(client: DgraphClient, schema: str):
    '''alter schema in DQL

    Example
    -------
    ```python
    schema = 'name: string @index(exact) .'
    ```
    Note
    ----
    Request using curl will return 
    ```json
    {
        "data": {
            "code": "Success",
            "message": "Done"
        }
    }
    ```
    but this operation return no useful data. 
    Related issue: https://discuss.dgraph.io/t/pydgraph-client-alter-schema-has-no-useful-information-in-response/18045 .
    '''
    op = pydgraph.Operation(schema=schema, run_in_background=False)
    r = client.alter(op)
    return r


def clean_cluster(client: DgraphClient):
    '''Drop all data including schema from the Dgraph instance.

    This operation will put Dgraph into a clean state.
    '''
    op = pydgraph.Operation(drop_all=True)
    client.alter(op)


def query(client: DgraphClient, q: str, variables: dict = None):
    """query the Dgraph database

    Parameters
    ----------
    client : DgraphClient
        DgraphClient client instance.
    q : str
        The text form of DQL query block.
    variables : Dict[str,str], optional
        Variables passed to the query, _variable names should be prefixed with "`$`"_.
        Variables only support string type for key and value.

    Example
    -------
    The following query
    ```go
    query all($a: string) {
        all(func: eq(name, $a)){
            name
        }
    }
    ```
    should be supplied with variables:
    ```python
    variables = {"$a": "Alice"}
    ```
    Note
    ----
    useful data and statistics in response:
    - json/rdf: depends on the `resp_format` query argument.
    - txn
    - latency
    - metrics 
    - uids
    """
    tx: Txn = None
    with Transaction(client, read_only=True, best_effort=True) as tx:
        response = tx.query(q, variables=variables)
    return response
   
# raw mutate
#   txn.mutate(set_obj=p)
# or
#   m = tx.create_mutation()
#   tx.create_request(mutations=[m], **kwargs)
#   tx.do_request(request) 
# no duplication check, easy to result duplicated nodes.
# pydgraph.Mutation 是gRPC消息对象的封装，没有公开暴露的接口

def insert(client:DgraphClient, key:str, value:str, 
           obj: dict = None, nquads: str = None, **kwargs):
    ''' insert a new node
    
    If the node with corresponding key exists, the insert operation fails.
    
    Note
    ----
    This operation is performed with the [upsert block](http://). Variables defined 
    in the query block can be used in the mutation blocks using the uid and val 
    functions.
    
    useful data and statistics in response:
    - json/rdf: depends on the `resp_format` query argument.
    - txn
    - latency
    - metrics 
    - uids: created node object in database.
    '''
    q = f"""
    query insert ($v:string) {{ 
        nodes as q(func: eq({key}, $v)) {{ uid name:x.name }} 
    }}
    """
    variables = {"$v": value}
    cond = "@if(eq(len(nodes), 0))"
    with Transaction(client, read_only=False) as tx:
        if obj is not None:
            m = tx.create_mutation(set_obj=obj, cond=cond)
        elif nquads is not None:
            m = tx.create_mutation(set_nquads=nquads, cond=cond)
        else:
            return # insert no data
        request = tx.create_request(query=q, variables=variables, mutations=[m], **kwargs)
        response = tx.do_request(request)  
    return response

def update(client:DgraphClient, key:str, value:str, 
           obj: dict = None, nquads: str = None, **kwargs):
    '''update an existing node.
    
    If the node with corresponding key does not exist or more than one nodes 
    are found, the operation will be canceled.
    
    Note
    ----
    This operation is performed with the [upsert block](http://). Variables defined 
    in the query block can be used in the mutation blocks using the uid and val 
    functions.
    '''
    q = f"""
    query insert ($v:string) {{ 
        nodes as q(func: eq({key}, $v)) {{ uid }} 
    }}
    """
    variables = {"$v": value}
    cond = "@if(eq(len(nodes), 1))"
    with Transaction(client, read_only=False) as tx:    
        if obj is not None:
            obj['uid'] = 'uid(nodes)'
        if nquads is not None:
            nquads = re.sub(r'_:.*?\s', 'uid(nodes)', nquads)
        m = tx.create_mutation(set_obj=obj, set_nquads=nquads, cond=cond)
        request = tx.create_request(query=q, variables=variables, mutations=[m], **kwargs)
        response = tx.do_request(request)  
    return response

def upsert(client: DgraphClient, key:str, value, 
           obj: dict = None, nquads: str = None, **kwargs):
    '''insert or update a node
    
    If the node with corresponding key does not exist, create a node with input
    data. Otherwise, update the existing node. If more than one nodes are found,
    the operation will be canceled.
    
    Note
    ----
    This operation is performed with the [upsert block](http://). Variables defined 
    in the query block can be used in the mutation blocks using the uid and val 
    functions.
    '''
    
    q = f"""
    query insert ($v:string) {{ 
        nodes as q(func: eq({key}, $v)) {{ uid }} 
    }}
    """
    variables = {"$v": value}
    with Transaction(client, read_only=False) as tx:
        mutations = []
        cond0 = "@if(eq(len(nodes), 0))"
        if obj is not None:
            m = tx.create_mutation(set_obj=obj, cond=cond0)
        elif nquads is not None:
            m = tx.create_mutation(set_nquads=nquads, cond=cond0)
        else:
            m = None # insert no data
        if m is not None:
            mutations.append(m)
        cond1 = "@if(eq(len(nodes), 1))"
        if obj is not None:
            obj['uid'] = "uid(nodes)"
        if nquads is not None:
            nquads = re.sub(r'_:.*?\s', '<{uid(nodes)}>', nquads)
        if obj is not None or nquads is not None:
            m = tx.create_mutation(set_obj=obj, set_nquads=nquads, cond=cond1)
            mutations.append(m)
        request = tx.create_request(query=q, variables=variables, mutations=mutations, **kwargs)
        response = tx.do_request(request)  
    return response

def delete(client:DgraphClient, key:str, value:str, fields:str = None, format='nquads',**kwargs):
    '''delete a node
    
    If the node with corresponding key does not exist or more than one nodes 
    are found, the operation will be canceled.
    
    Note
    ----
    This operation is performed with the [upsert block](http://). Variables defined 
    in the query block can be used in the mutation blocks using the uid and val 
    functions.
    
    If the node has no corresponding ``dgraph.type``, then ``fields`` must be specified.
    Otherwise, Dgraph cannot determine which predicates to delete.
    In addition, if the node has extra fields not declared in its ``dgraph.type``, then
    these fields will not be deleted if we do not explicitly specify it in ``fields``.
    '''
    
    q = f"""
    query insert ($v:string) {{ 
        nodes as q(func: eq({key}, $v))
    }}
    """  #  {{ uid }} 
    variables = {"$v": value}
    cond = "@if(eq(len(nodes), 1))"
    tx: Txn = None
    with Transaction(client, read_only=False) as tx:
        if format == 'nquads':
            if fields is None:
                nquads = 'uid(nodes) * * .'
            else:
                nquads = '\n'.join([f'uid(nodes) <{f}> * .' for f in fields ]) 
            m = tx.create_mutation(del_nquads=nquads, cond=cond)
        else:
            obj = {"uid": "uid(nodes)"}
            if fields is not None:
                for f in fields:
                    obj[f] = None
            m = tx.create_mutation(del_obj=obj)
        request = tx.create_request(query=q, variables=variables, 
                                    mutations=[m], **kwargs)
        response = tx.do_request(request)
    return response


if __name__ == '__main__':
    with contextlib.closing(pydgraph.DgraphClientStub('localhost:9080')) as client_stub:
        client = DgraphClient(client_stub)
        q = '''
        {
            company(func: type(x.Company), first: 10){
                    expand(_all_)
            }
        }
        '''
        data = query(client, q)
        print(data)
        