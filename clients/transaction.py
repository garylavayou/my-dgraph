import contextlib
from pydgraph import DgraphClient


@contextlib.contextmanager
def Transaction(client: DgraphClient,
                read_only: bool = False, best_effort: bool = False,
                timeout=None, metadata=None, credentials=None):
    tx = client.txn(read_only=read_only, best_effort=best_effort)
    try:
        yield tx
        if read_only == False and tx._finished == False:
            tx.commit(timeout=timeout, metadata=metadata,
                      credentials=credentials)
    except Exception as e:
        raise e
    finally:
        tx.discard(timeout=timeout, metadata=metadata, credentials=credentials)
