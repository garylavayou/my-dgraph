'''
TODO: High-level Dgraph Query Language Interface
'''

from typing import List, Union

from pydgraph import Txn

from .transaction import Transaction


class QueryFacet:
    _type:str   # 'facet' or 'filter'
    _filter:str
    _name:str
    _alias:str  # if alias is set, it will be return as the parent's data field
    _order:str
    
    def __init__(self) -> None:
        pass
    
    def __str__(self) -> str:
        pass

class Predicate:
    _name:str
    _alias:str
    _var:str  # return as a variable
    _facets: List[QueryFacet] 
    # syntax: "alias : var as name @facets() ... "
    

class Function:
    _name: str
    _arguments: List[str]
    
    def __init__(self, name:str, **arguments):
        self._name = name
        self._arguments = arguments
        pass
    
class QueryFunction(Function):
    def __init__(self, name: str, *arguments:str):
        super().__init__(name, *arguments)
 
class Filter:
    _op: str # AND, OR, NOT
    _sub_filters: List[Function]
    
    def __init__(self, operator:str='AND', *filters:Function) -> None:
        self._op = operator.upper()
        self._sub_filters = filters
    
    def __str__(self):
        if self._sub_filters == 0:
            return ''
        if self._sub_filters == 1:
            if self._op == 'NOT':
                return f'NOT ({self._sub_filters[0]})'
            else:
                return str(self._sub_filters[0])
        if self._op in ('AND', 'NOT'):
            repr = ' AND '.join([f'({f})' for f in self._sub_filters])
        elif self._op == 'OR':
            repr = ' OR '.join([str(f) for f in self._sub_filters])
        if self._op == 'NOT':
            repr = f"NOT ({repr})"
        return repr                
            
class NodeQuery(Predicate):
    ''' the general node query class
    
    A node query can contains a list of sub-node-queries and a list of data predicates.
    '''
    _name: str
    _queries: list
    _fields: List[Predicate]
    _facets: List[QueryFacet]
    _query_args: List[dict]
    _body: List[Predicate]
    _filters: List[Filter]
    _cascade: bool
    _normalize: bool
    # syntax: "alias : var as name @facets() ...{sub_queries} "
    def __init__(self, name:str='q', 
                 body: List[Predicate] = None, 
                 orderasc:str=None, orderdesc:str=None,
                 offset:int=None, first:int=None, after:str=None, 
                 filters: List[Filter]=None, 
                 cascade:bool=False, 
                 normalize:bool=False) -> None:
        self._name = name
        self._query_args = []
        if orderasc is not None:
            self._query_args.append({'orderasc': orderasc})
        elif orderdesc is not None:
            self._query_args.append({'orderdesc': orderdesc})
        if offset is not None:
            self._query_args.append({'offset': offset})
        if first is not None:
            self._query_args.append({'first': first})
        if after is not None:
            self._query_args.append({'after': after})
        self._filters = filters    
        self._body = body
        self._cascade = cascade
        self._normalize = normalize
   
class RootNodeQuery(NodeQuery):
    '''The node query class that should be used in DgraphQuery.'''
    def __init__(self, 
                 func:QueryFunction, 
                 name: str = 'q', 
                body: List[Union[Predicate,NodeQuery]] = None, 
                order: str = None, 
                offset: int = None, first: int = None, after: str = None, 
                filters: List[str] = None, 
                cascade: bool = False, 
                normalize: bool = False
                ) -> None:
        super().__init__(name, body, order, offset, first, after, filters, cascade, normalize)
        self._query_args.insert(0, {'func': func})
        
class Argument:
    _name: str
    _type: str
    _default: str
    
    def __init__(self, name:str, type:str='string', default:str=None) -> None:
        if not name.startswith('$'):
            name = "$"+name
        self._name = name
        self._type = type
        self._default = default
        
    def __str__(self) -> str:
        repr = '{0}:{1}'.format(self._name, self._type)
        if self._default is not None:
            repr = f'{repr} = "{self._default}"'
        return repr
    
       
     
class DgraphQuery:
    '''contains one or more node query objects
    '''
    _queries: List[RootNodeQuery]
    def __init__(self, name:str='q', arguments:List[Argument]=None) -> None:
        pass
    
    def add_query(self, query:RootNodeQuery):
        pass
    
    def __str__(self):
        pass
    
    def _check_variables(self):
        '''check if any arguments declared can be found in input variables'''
        pass
    
    def request(self, client, variables:dict=None):
        tx: Txn = None
        with Transaction(client, read_only=True, best_effort=True) as tx:
            response = tx.query(self.to_string(), variables=variables)
        return response

def create_query(name:str='q', arguments:dict=None, 
                 order:str=None, offset:int=None, first:int=None, after:str=None, 
                 filters: List[str]=None, cascade:bool=False, normalize:bool=False):
    '''parse string representation to query structure, and then convert to query expression'''
